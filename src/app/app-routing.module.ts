import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { authRoutes, AuthGuard } from './auth/auth.routes';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  ...authRoutes,
  { path: "", component: LandingComponent, canActivate: [AuthGuard]},
  { path: "**", pathMatch: "full", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot( routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
