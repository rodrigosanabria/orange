import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers, getInitialState } from '.';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { UserEffects } from './effects/user.effects';
import { StorageModule } from '../storage/storage.module';


@NgModule({
  declarations: [
  ],
  imports: [
    StoreModule.forRoot(reducers, { initialState: getInitialState }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects, UserEffects]),
    StorageModule
  ],
  providers: []
})
export class StateModule { }
