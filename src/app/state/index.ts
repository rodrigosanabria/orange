import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
  } from '@ngrx/store';
  import { environment } from '../../environments/environment';
  import * as fromCounter from './reducers/counter.reducer';
  import* as fromUser from './reducers/user.reducer';



  
  export interface State {
  
    counter: fromCounter.State;
    user: fromUser.UserState
  }
  
  export const reducers: ActionReducerMap<State> = {
  
    counter: fromCounter.reducer,
    user: fromUser.reducer
  };

  const initialState = { counter: 0, user: {isLogged: false} };

  export function getInitialState() {
    return  initialState;
  }
  
  
  export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

  