import { Action } from "@ngrx/store";

export interface CounterAction extends Action {
    payload?: any;
}