import { Action } from "@ngrx/store";

export interface User {
    username : string
    password: string
}

export interface UserAction extends Action {
    payload?: User;
}