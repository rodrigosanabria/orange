import { Action } from '@ngrx/store';
import { UserAction, User } from '../actions/user.action';


export interface UserState {
    loggedUser?: User
    isLogged: boolean
}


export function reducer(state: UserState, action: UserAction): UserState {
  switch (action.type) {
    case "LOG_IN":
      return  Object.assign({},state, action.payload)
    case "LOG_IN_SUCCESS":
      return {loggedUser: action.payload, isLogged : true}
    case "LOG_IN_FAILED":
      return {isLogged: false}
    case "LOG_OUT":
    return {isLogged: false}
    case "RESTORE_SESSION":
    return {loggedUser: action.payload, isLogged : true}
    default:
      return state;
  }
}
