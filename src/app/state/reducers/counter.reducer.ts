import { Action } from '@ngrx/store';
import { CounterAction } from '../actions/counter.action';


export interface State {
}


export const initialState: State = {
counter: 0
};

export function reducer(state: number, action: CounterAction): State {
  switch (action.type) {
    case "INCREMENT":
        if (action.payload)
      return  state+(1*action.payload);
    default:
      return state;
  }
}
