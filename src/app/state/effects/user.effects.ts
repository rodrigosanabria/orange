import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { UserAction, User } from '../actions/user.action';
import { StorageService } from '../../storage/storage.service';

const user: User = { username: "admin", password: "1234" };

@Injectable()
export class UserEffects {

  constructor(private actions$: Actions, private storageS: StorageService) { 
      
  }

  @Effect() login$: Observable<UserAction> = this.actions$.pipe(
    ofType('LOG_IN'),
    mergeMap((action: UserAction) => {
        if (action.payload.username === user.username ) {
            if (action.payload.password === user.password) {
                    this.storageS.local.set("user", action.payload)
                return of({type: "LOG_IN_SUCCESS", payload: action.payload})
            }
        }
            return of({type: "LOG_IN_FAILED"});
    }
    )
  );

  @Effect() logout$: Observable<UserAction> = this.actions$.pipe(
    ofType('LOG_OUT'),
    mergeMap((action: UserAction) => {
            this.storageS.local.remove("user");
            return of({type: "LOG_OUT"});
    }
    )
  );
  @Effect({ dispatch: false }) restore$: Observable<UserAction> = this.actions$.pipe(
    ofType('RESTORE_SESSION'),
    tap((action: UserAction) => {
            //CHECK SESSION VALIDITY
    }
    )
  );
  
}