import { Injectable } from '@angular/core';
const id = "orange-"

class Local {
    public set(key, value) {
        key = id+key;
        localStorage.setItem(key,JSON.stringify(value));
    }
    public get(key) {
        key = id+key;
        return JSON.parse(localStorage.getItem(key));
    }
    public remove(key) {
        key = id+key;
        localStorage.removeItem(key);
    }
    
}

class Session {

    public set(key, value) {
        key = id + key;
        sessionStorage.setItem(key,JSON.stringify(value));
    }
    public get(key) {
        key = id + key;
        return JSON.parse(sessionStorage.getItem(key));
    }
    public remove(key) {
        key = id + key;
        sessionStorage.removeItem(key);
    }   
}

@Injectable()
export class StorageService {

    local: Local = new Local();
    session : Session = new Session();

    constructor() { 
      console.log("STORAGE", this.local.get("prueba"))
    }

}
