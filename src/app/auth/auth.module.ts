import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';;
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { StorageModule } from '../storage/storage.module';
import { AuthGuard } from './auth.routes';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StorageModule
  ],
  declarations: [LoginComponent],
  providers: [AuthService, AuthGuard]
})
export class AuthModule { }
