import { Injectable } from '@angular/core';
import { UserAction, User } from '../state/actions/user.action';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { StorageService } from '../storage/storage.service';
import { UserState } from '../state/reducers/user.reducer';

@Injectable()
export class AuthService {

  public loggedUser$: Observable<UserState>;
  public isLogged = false;

  constructor(private store: Store<User>, private storageS: StorageService) { 
    this.loggedUser$ = store.pipe(select("user"));
    let storedUser: User = storageS.local.get("user");
    console.log("STORED",storedUser);
      if (storedUser) {
          this.dispatch("RESTORE_SESSION",storedUser)
      }

      this.loggedUser$.subscribe(state=> {
        console.log("USER_STATE", state);
        this.isLogged = state.isLogged;
      })
   }

   dispatch(type: string, payload:User = null) {
     this.store.dispatch<UserAction>({type,payload})
   }

}
