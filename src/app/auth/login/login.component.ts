import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User, UserAction } from '../../state/actions/user.action';
import { Store, select } from '@ngrx/store';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: FormGroup;
  user$: Observable<User>;
  constructor(private fb: FormBuilder, private authS: AuthService) {
   
/*     store.dispatch<CounterAction>({type: "INCREMENT", payload: 2}); */
  }

  ngOnInit() {
    this.user = this.fb.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]]

    })
  }

  onSubmit ({value, valid}: {value: User, valid: boolean}) {
    if (valid) {
      this.authS.dispatch("LOG_IN",value)
    }
  }

}
