import { Routes, CanActivate, Router } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { Injectable } from "@angular/core";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
    isLogged: boolean = false;
    constructor(private authS: AuthService, private router: Router) {}

    canActivate(){
        if (this.authS.isLogged) {
        return true
        } else {
            this.router.navigateByUrl("/login")
        }
    }
}

export const authRoutes : Routes = [
    {path: "login", component: LoginComponent}
]